<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
<?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?>
<h3 class="title"><?php print $title; ?></h3>
<?php if ($picture) print $picture; ?>
    
    <div class="content"><?php print $content; ?></div>
    <div class="meta"> <span class="commenter"> <?php print (theme('username', $node)) ?> </span> <span class="date"> <?php print ( format_date($node->created, 'custom', 'd')) ?> <?php print (format_date($node->created, 'custom', 'M')) ?> <?php print (format_date($node->created, 'custom', 'Y')) ?> </span> <?php print $links; ?></div>
</div>
