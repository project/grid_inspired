<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<?php /* different ids allow for separate theming of the home page */ ?>
<body class="<?php print $body_classes; ?>">
  <div id="page">
    <div id="header">
      <div id="masthead">

        <?php if ($logo): ?>
          <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
          </a>
        <?php endif; ?>

        <div id="name-and-slogan">

        <?php if ($site_name): ?>
          <h1 id='site-name'>
            <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
              <?php print $site_name; ?>
            </a>
          </h1>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id='site-slogan'>
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>

        </div> <!-- /name-and-slogan --><br />

      </div> <!-- /masthead -->


      <div id="navigation" class="menu <?php if ($primary_links) { print "withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
	  <!--[if IE]><div id="IEroot"><![endif]--><?php print $search_box; ?><!--[if IE]></div><![endif]-->

          <?php if ($primary_links): ?>
            <div id="primary" class="clear-block">
              <?php print theme('links', $primary_links); ?>
            </div>
          <?php endif; ?>

          <?php if ($secondary_links): ?>
            <div id="secondary" class="clear-block">
              <?php print theme('links', $secondary_links); ?>
            </div>
          <?php endif; ?>
      </div> <!-- /navigation -->

    </div> <!-- /header -->

    <div id="container" class="clear-block">

      <div id="main" class="column"><div id="squeeze">
        <?php if ($breadcrumb): ?><?php print $breadcrumb; ?><?php endif; ?>
        <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
        <?php if ($above_content_region):?><div id="content-top"><?php print $above_content_region; ?></div><?php endif; ?>
        <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
        <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
        <?php print $help; ?>
        <?php print $messages; ?>
        <?php print $content; ?>
        <?php if ($below_content_region): ?><div id="content-bottom"><?php print $below_content_region; ?></div><?php endif; ?>
      </div></div> <!-- /squeeze /main -->

      <?php if ($left_region): ?>
        <div id="sidebar-left" class="column sidebar">
          <?php print $left_region; ?>
        </div> <!-- /sidebar-left -->
      <?php endif; ?>

      <?php if ($right_region): ?>
        <div id="sidebar-right" class="column sidebar">
          <?php print $right_region; ?>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>

    </div> <!-- /container -->

    <?php if ($footer_menu): ?> <div id="footer-wrapper"> <div id="footer-menu" class="clear-block"> <?php print $footer_menu; ?> </div> </div> <?php endif; ?>

    <div id="footer-region">

      <?php if ($footer_left): ?> <div id="footer-left"> <?php print $footer_left; ?> </div> <?php endif; ?>
      <?php if ($footer_mid): ?> <div id="footer-mid"> <?php print $footer_mid; ?> </div> <?php endif; ?>
      <?php if ($footer_right): ?> <div id="footer-right"> <?php print $footer_right; ?> </div> <?php endif; ?>

      <div id="footer">
        <?php print $footer_message; ?> <?php if ($feed_icons): ?><span id="subscribe-icon">Subscribe to this feed: <?php print $feed_icons; ?></span><?php endif; ?> <p><a href="http://5thirtyone.com/grid-focus" title="Grid Focus by: Derek Punsalan">Grid Focus</a> theme by <a href="http://5thirtyone.com">Derek Punsalan</a>, ported to <a href="http://drupal.org">Drupal</a> by <a href="http://neemtree.com.au">Neem Tree</a>.</p>
      </div> <!-- /footer -->
    </div> <!-- /footer-wrapper -->

    <?php print $closure; ?>

  </div> <!-- /page -->

</body>
</html>