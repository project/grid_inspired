<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>" id="node-<?php print $node->nid; ?>">

  <?php if ($submitted): ?> <div class="submitted"><span class="date"><?php print ( format_date($node->created, 'custom', 'd')) ?> <?php print (format_date($node->created, 'custom', 'M')) ?> <?php print (format_date($node->created, 'custom', 'Y')) ?></span> <span class="author"><?php print (theme('username', $node)) ?></span></div>  <?php endif; ?>

  <?php if ($page == 0): ?> <h2 class="title"> <a href="<?php print $node_url ?>"><?php print $title ?></a> </h2> <?php endif; ?>

  <?php if ($picture) print $picture ?>  
  
  <div class="content"> <?php print $content?> </div>

  <?php if ($page != 0): ?> <div class="meta"> 
	<?php if ($has_terms) : ?> <div class="taxonomy"><?php print $terms ?></div> <?php endif; ?> 

	<?php if ($links): ?> <div class="links"> <?php print $links ?> </div> <?php endif; ?>
  </div> <?php endif; ?>
  
</div>
